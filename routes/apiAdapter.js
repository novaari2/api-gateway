const axios = require('axios');
const {TIMEOUT} = process.env

module.exports = (baseUrl) => {
    return axios.create({
        baseURL: baseUrl,
        timeout: TIMEOUT
    })
}


//module ini digunakan untuk menghubungkan api gateway ke service